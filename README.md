# matplotlib2tikz-latex-docker

## Python: Make Figure

If you already have `numpy` and `matplotlib` (hopefully in a virtual environment, but maybe in the base Python install), just do `pip install matplotlib2tikz` and `python try_matplotlib2tikz.py`

If you don't have `numpy` installed, it's kind of a pain to compile, so here we will use an environment via conda.
```
conda env create -f environment.yml
conda activate tikz
python try_matplotlib2tikz.py
conda deactivate
```

## LaTeX: Make PDF

After the `test.tex` file is created, it can be included in a LaTeX document. If you want to use OverLeaf or MikTex, go ahead. I wanted to use Docker image that could compile LaTeX on my laptop. The command below will be slow the first time due to downloading the image, but then it just works.
```
docker run --rm -v $PWD:/workdir ivanpondal/alpine-latex:latest pdflatex simple.tex
```